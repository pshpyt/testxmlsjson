class ExamplesController < ApplicationController
  before_action :set_example, only: [:show, :update, :destroy]

  # GET /examples
  # GET /examples.json
  def results
     mappings = {"param_1" => "some_string", "param_2" => "some_date", "some_code" => "iso_code", "some_price_param"=>"price"}
     #response = HTTParty.get('http://localhost:3333/')
     source =  params[:source]? params[:source] : 'http://localhost:3333/'
     response = HTTParty.get(source)
     #hash = Hash.from_xml(response.body)
     examples = {}
     xml = Nokogiri::XML(response.body)
     xml.xpath("//example").each do |example|
       el_hash = Hash.from_xml(example.to_s)
       el_hash.keys.each{ |k| el_hash[ mappings[k] ] = el_hash.delete(k) if mappings[k] }
       examples << el_hash
     end
    
    render json: new_hash.to_json 
  end
  
  
  def index
    @examples = Example.all

    render xml: @examples
  end

  # GET /examples/1
  # GET /examples/1.json
  def show
    render json: @example
  end

  # POST /examples
  # POST /examples.json
  def create
    @example = Example.new(example_params)

    if @example.save
      render json: @example, status: :created, location: @example
    else
      render json: @example.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /examples/1
  # PATCH/PUT /examples/1.json
  def update
    @example = Example.find(params[:id])

    if @example.update(example_params)
      head :no_content
    else
      render json: @example.errors, status: :unprocessable_entity
    end
  end

  # DELETE /examples/1
  # DELETE /examples/1.json
  def destroy
    @example.destroy

    head :no_content
  end

  private

    def set_example
      @example = Example.find(params[:id])
    end

    def example_params
      params[:example]
    end
end
