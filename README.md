# README #

To perform this test - run this application on localhost port 3333

# The test task #
Write a separate API application including all relevant tests that performs the following:

1. Your application should respond to request sent to http://localhost:3000/example
1. Please keep your Gemfile as short as possible.
1. connects to this application on localhost  port 3333
1. read result in xml format 
1. return json with ***replaced key names*** as following:
```
#!
+--------------------+-------------+
|  original value    |  new value  |
+--------------------+-------------+
| param_1            | some_string |
| param_2            | some_date   |
| some_code          | iso_code    |
| some_price_param   | price       |
+--------------------+-------------+
```

#Tests#
1. Your code and tests should work with different input xmls
1. The tests should work with and without mocking

* Commit your code to some repository and send me the details.
 
Estimated time: about 30-40 minutes