require 'spec_helper'
require 'rails_helper'
describe ExamplesController do
  describe "GET 'results'" do
    it "return results in json" do
      get 'results'
	    source = File.open("spec/examples1.xml").read
      xml = Nokogiri::XML(source)
      results = JSON.parse(response.body)
      #binding.pry
	    response.should be_success
      expect(results.size).to eq(xml.xpath("//id").size)
      expect(results.first[1]["name"]).to eq(xml.xpath("//name").first.text)
      expect(results.first[1]["iso_code"]).to eq(xml.xpath("//some_code").first.text)
    end
  end

end
